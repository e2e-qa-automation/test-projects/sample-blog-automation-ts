import fs from 'fs'
import path from 'path'

export const createPathDirectories = (p: string): void => {
  const directories: string[] = p.split('/')
  for (let i = 0; i <= directories.length; i++) {
    const directory = path.resolve(directories.slice(0, i).join('/'))
    if (!fs.existsSync(directory)) {
      fs.mkdirSync(directory)
    }
  }
}
