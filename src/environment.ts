import { IBrowser } from 'selenium-webdriver/lib/capabilities'

interface IEnvironment {
  browserName?: string
  seleniumHubHost?: string
}

export const environment: IEnvironment = {
  browserName: process.env.BROWSER_NAME,
  seleniumHubHost: process.env.SELENIUM_HUB_HOST,
}
